package me.jfenn.coordshud.impl

import eu.pb4.placeholders.api.PlaceholderContext
import eu.pb4.placeholders.api.PlaceholderResult
import eu.pb4.placeholders.api.Placeholders
import me.jfenn.coordshud.api.IPlaceholderApi
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text
import net.minecraft.util.Identifier
import org.slf4j.Logger

class PlaceholderApi(
    private val log: Logger,
) : IPlaceholderApi {
    override fun parseText(format: Text, player: ServerPlayerEntity): Text {
        return Placeholders.parseText(format, PlaceholderContext.of(player))
    }

    override fun registerPlaceholder(id: Identifier, callback: (ServerPlayerEntity) -> Text) {
        Placeholders.register(id) { ctx, _ ->
            val player = ctx.player ?: return@register PlaceholderResult.invalid()
            val text = try {
                callback(player)
            } catch (e: Exception) {
                log.error("Error providing placeholder $id", e)
                return@register PlaceholderResult.invalid()
            }
            PlaceholderResult.value(text)
        }
    }
}