package me.jfenn.coordshud.mixin;

import me.jfenn.coordshud.common.mixinhelper.NetworkMixinHelper;
import net.minecraft.network.PacketCallbacks;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.GameMessageS2CPacket;
import net.minecraft.network.packet.s2c.play.OverlayMessageS2CPacket;
import net.minecraft.server.network.ServerCommonNetworkHandler;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerCommonNetworkHandler.class)
public abstract class ServerCommonNetworkHandlerMixin {
    @Inject(method = "send(Lnet/minecraft/network/packet/Packet;Lnet/minecraft/network/PacketCallbacks;)V", at = @At("HEAD"))
    public void send(Packet<?> packet, PacketCallbacks callbacks, CallbackInfo ci) {
        if ((Object)this instanceof ServerPlayNetworkHandler playNetworkHandler) {
            if (packet instanceof GameMessageS2CPacket messagePacket && messagePacket.overlay()) {
                NetworkMixinHelper.INSTANCE.onSendMessagePacket(playNetworkHandler.player, messagePacket.content());
            }
            if (packet instanceof OverlayMessageS2CPacket messagePacket) {
                NetworkMixinHelper.INSTANCE.onSendMessagePacket(playNetworkHandler.player, messagePacket.getMessage());
            }
        }
    }
}
