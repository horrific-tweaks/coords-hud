package me.jfenn.coordshud.common

import me.jfenn.coordshud.api.IPlaceholderApi
import me.jfenn.coordshud.api.ITextSerializer
import me.jfenn.coordshud.api.commands.ICommandManager
import me.jfenn.coordshud.common.cache.PlayerCache
import me.jfenn.coordshud.common.cache.PlayerCacheController
import me.jfenn.coordshud.common.commands.CoordsHudCommand
import me.jfenn.coordshud.common.config.ConfigService
import me.jfenn.coordshud.common.db.ConnectionFactory
import me.jfenn.coordshud.common.db.PlayerDao
import me.jfenn.coordshud.common.hud.HudController
import me.jfenn.coordshud.common.hud.HudState
import me.jfenn.coordshud.common.hud.PlaceholderController
import org.slf4j.Logger

fun commonModule(
    logger: Logger,
    placeholderApi: IPlaceholderApi,
    textSerializer: ITextSerializer,
    commandManager: ICommandManager,
) {
    val database = ConnectionFactory(logger).connect()
    val config = ConfigService(logger).readConfig()
    val playerCache = PlayerCache()
    val playerDao = PlayerDao(database, playerCache)

    PlayerCacheController(playerCache, playerDao)

    val hudState = HudState()
    HudController(
        logger = logger,
        playerDao = playerDao,
        cache = playerCache,
        placeholderApi = placeholderApi,
        textSerializer = textSerializer,
        config = config,
        state = hudState,
    )

    PlaceholderController(placeholderApi)

    CoordsHudCommand(
        playerDao = playerDao,
        textSerializer = textSerializer,
        config = config,
        state = hudState,
        commandManager = commandManager,
    )
}
