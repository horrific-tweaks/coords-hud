package me.jfenn.coordshud.common.mixinhelper

import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

object NetworkMixinHelper {
    private val lastNonCoordsMessage = WeakHashMap<ServerPlayerEntity, Int>()

    private val sentMessages = Collections.newSetFromMap(ConcurrentHashMap<String, Boolean>())

    fun tick() {
        sentMessages.clear()
    }

    /**
     * Invoked when sending an overlay message from CoordsHUD
     */
    fun onSendingText(text: Text) {
        sentMessages.add(text.toString())
    }

    /**
     * Invoked when the mixin intercepts an outgoing overlay message packet
     */
    fun onSendMessagePacket(player: ServerPlayerEntity, text: Text) {
        if (text.string.isBlank())
            return

        if (sentMessages.contains(text.toString())) {
            // The message was sent by CoordsHud; ignore
            return
        }

        lastNonCoordsMessage[player] = player.server.ticks
    }

    /**
     * Calculates a Duration since the last non-coordshud message that
     * was sent to this player
     */
    fun getLastMessage(player: ServerPlayerEntity): Duration {
        val lastMessageTick = lastNonCoordsMessage[player]
            ?: return Duration.INFINITE

        // Assuming 50 MSPT
        return ((player.server.ticks - lastMessageTick) * 50).milliseconds
    }

    init {
        // clean up when server closes
        ServerLifecycleEvents.SERVER_STOPPED.register {
            lastNonCoordsMessage.clear()
            sentMessages.clear()
        }
    }
}