package me.jfenn.coordshud.common.config

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.buildJsonArray
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import me.jfenn.coordshud.common.MOD_ID

@Serializable
data class CoordsHudConfig(
    val isEnabledByDefault: Boolean = true,
    val defaultFormat: JsonElement = DEFAULT_FORMAT,
) {
    companion object {
        val DEFAULT_FORMAT = buildJsonArray {
            add(buildJsonObject {
                put("text", "XYZ: ")
                put("color", "gold")
            })
            add(buildJsonObject {
                put("text", "%player:pos_x 0% %player:pos_y 0% %player:pos_z 0%   ")
                put("color", "white")
            })
            add(buildJsonObject {
                put("text", "%$MOD_ID:compass_icon% ")
                put("color", "gold")
            })
            add(buildJsonObject {
                put("text", "%$MOD_ID:compass_facing%  ")
                put("color", "white")
            })
            add(buildJsonObject {
                put("text", "%$MOD_ID:time_icon% ")
                put("color", "gold")
            })
            add(buildJsonObject {
                put("text", "%world:time%")
                put("color", "white")
            })
        }
    }
}