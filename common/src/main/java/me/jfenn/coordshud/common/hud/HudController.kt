package me.jfenn.coordshud.common.hud

import me.jfenn.coordshud.api.IPlaceholderApi
import me.jfenn.coordshud.api.ITextSerializer
import me.jfenn.coordshud.common.cache.PlayerCache
import me.jfenn.coordshud.common.config.CoordsHudConfig
import me.jfenn.coordshud.common.config.json
import me.jfenn.coordshud.common.db.PlayerDao
import me.jfenn.coordshud.common.db.PlayerRecord
import me.jfenn.coordshud.common.mixinhelper.NetworkMixinHelper
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents
import net.minecraft.server.MinecraftServer
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text
import org.slf4j.Logger
import kotlin.time.Duration.Companion.seconds

class HudController(
    private val logger: Logger,
    private val playerDao: PlayerDao,
    private val cache: PlayerCache,
    private val placeholderApi: IPlaceholderApi,
    private val textSerializer: ITextSerializer,
    private val config: CoordsHudConfig,
    private val state: HudState,
) {

    private val defaultFormat = textSerializer.fromJson(
        json.encodeToString(config.defaultFormat)
    )

    private fun getPlayerFormat(record: PlayerRecord): Text {
        return cache.playerFormats.get(record.uuid) {
            record.getFormatText(textSerializer, logger)
                ?: defaultFormat
        }
    }

    private fun getPlayerMessage(player: ServerPlayerEntity): Text? {
        val record = playerDao.getPlayer(player.uuid)
        if (!record.isEnabled(config)) return null

        val format = getPlayerFormat(record)
        return placeholderApi.parseText(format, player)
    }

    private fun shouldResendTitle(player: ServerPlayerEntity, messageJson: String): Boolean {
        val (prevTicks, prevMessage) = state.titleTracker[player] ?: return true
        return (
            // if 30 ticks have passed since the last message...
            prevTicks + 30 < player.server.ticks ||
            // or the message has changed...
            prevMessage != messageJson
        ) && (
            // and at least 2 ticks have passed since the last message
            prevTicks + 2 <= player.server.ticks
        )
    }

    private fun sendHuds(server: MinecraftServer) {
        for (player in server.playerManager.playerList) {
            // If the player has received a non-coordshud overlay within 3 seconds, don't send HUD messages
            // (this avoids the mod from hiding messages from other mods / vanilla behavior, e.g. waiting to sleep messages)
            if (NetworkMixinHelper.getLastMessage(player) < 3.seconds)
                continue

            val message = getPlayerMessage(player) ?: continue
            val messageJson = message.toString()
            if (shouldResendTitle(player, messageJson)) {
                NetworkMixinHelper.onSendingText(message)
                player.sendMessage(message, true)

                // Insert the sent title + tick into the titleTracker
                state.titleTracker[player] = Pair(server.ticks, messageJson)
            }
        }

        // Clean up tracker entries for players that are no longer
        // in the playerList
        val inverseKeys = state.titleTracker.keys - server.playerManager.playerList.toSet()
        for (player in inverseKeys) {
            state.titleTracker.remove(player)
        }
    }

    init {
        ServerTickEvents.START_SERVER_TICK.register { server ->
            NetworkMixinHelper.tick()
            sendHuds(server)
        }
    }

}