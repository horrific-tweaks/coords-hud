package me.jfenn.coordshud.common.cache

import me.jfenn.coordshud.common.db.PlayerDao
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents

class PlayerCacheController(
    private val playerCache: PlayerCache,
    private val playerDao: PlayerDao,
) {

    init {
        ServerPlayConnectionEvents.JOIN.register { handler, sender, server ->
            // on login, clear the player's cache and re-fetch the player record
            playerCache.remove(handler.player.uuid)
            playerDao.getPlayer(handler.player.uuid)
        }

        ServerPlayConnectionEvents.DISCONNECT.register { handler, server ->
            // on disconnect, clear the player's cache
            playerCache.remove(handler.player.uuid)
        }
    }

}