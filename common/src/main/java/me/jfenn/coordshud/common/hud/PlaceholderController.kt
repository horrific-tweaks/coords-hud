package me.jfenn.coordshud.common.hud

import me.jfenn.coordshud.api.IPlaceholderApi
import me.jfenn.coordshud.common.MOD_ID
import net.minecraft.text.Text
import net.minecraft.util.Identifier
import kotlin.math.roundToInt

class PlaceholderController(
    placeholderApi: IPlaceholderApi,
) {

    companion object {
        val COMPASS_ARROWS = arrayOf(
            "\uD83E\uDC83", // S
            "\uD83E\uDC86", // SE
            "\uD83E\uDC82", // E
            "\uD83E\uDC85", // NE
            "\uD83E\uDC81", // N
            "\uD83E\uDC84", // NW
            "\uD83E\uDC80", // W
            "\uD83E\uDC87", // SW
        )

        const val ICON_SUN = "\uD83C\uDF1E"
        const val ICON_SUNRISE = "\uD83C\uDF05"
        const val ICON_MOON = "\uD83C\uDF19"

        val COMPASS_ICON_ID = Identifier.of(MOD_ID, "compass_icon")!!
        val COMPASS_FACING_ID = Identifier.of(MOD_ID, "compass_facing")!!
        val TIME_ICON_ID = Identifier.of(MOD_ID, "time_icon")!!
    }

    init {
        placeholderApi.registerPlaceholder(COMPASS_ICON_ID) { player ->
            var yaw = player.yaw
            while (yaw < 0) yaw += 360
            val index = (yaw / 45f).roundToInt() % 8
            Text.literal(COMPASS_ARROWS[index])
        }

        placeholderApi.registerPlaceholder(COMPASS_FACING_ID) { player ->
            val facing = player.horizontalFacing.name[0].uppercase()
            Text.literal(facing)
        }

        placeholderApi.registerPlaceholder(TIME_ICON_ID) { player ->
            val server = player.server
            val hour = (server.overworld.timeOfDay / 1000 + 6) % 24
            val icon = when {
                hour in 6..17 -> ICON_SUN
                hour < 5 || hour > 18 -> ICON_MOON
                else -> ICON_SUNRISE
            }
            Text.literal(icon)
        }
    }
}
