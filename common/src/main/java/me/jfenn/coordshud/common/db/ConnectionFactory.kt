package me.jfenn.coordshud.common.db

import MigrationUtils
import me.jfenn.coordshud.common.MOD_ID
import net.fabricmc.api.EnvType
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents
import net.fabricmc.loader.api.FabricLoader
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.Logger
import kotlin.io.path.absolutePathString

class ConnectionFactory(
    private val log: Logger,
) {

    private val defaultUrl = FabricLoader.getInstance()
        .configDir
        .absolutePathString()
        .let { configDir ->
            "jdbc:h2:file:$configDir/$MOD_ID/data.h3"
        }

    private fun doMigrations(
        db: Database,
    ) {
        val tablesList = arrayOf<Table>(PlayerTable)
        transaction(db) {
            val changes = MigrationUtils.statementsRequiredForDatabaseMigration(*tablesList)

            for (change in changes) {
                log.debug(change)
                exec(change)
            }
        }
    }


    fun connect(
        url: String = defaultUrl,
    ) : Database {
        val db = Database.connect(url)
        doMigrations(db)

        // Clean up db when the server/client stops...
        when (FabricLoader.getInstance().environmentType) {
            EnvType.CLIENT -> {
                ClientLifecycleEvents.CLIENT_STOPPING.register {
                    TransactionManager.closeAndUnregister(db)
                }
            }
            EnvType.SERVER -> {
                ServerLifecycleEvents.SERVER_STOPPED.register {
                    TransactionManager.closeAndUnregister(db)
                }
            }
            else -> {}
        }

        return db
    }

}