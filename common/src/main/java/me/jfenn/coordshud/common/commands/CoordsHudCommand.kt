package me.jfenn.coordshud.common.commands

import me.jfenn.coordshud.api.ITextSerializer
import me.jfenn.coordshud.api.commands.ICommandManager
import me.jfenn.coordshud.common.config.CoordsHudConfig
import me.jfenn.coordshud.common.db.PlayerDao
import me.jfenn.coordshud.common.hud.HudState
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text

class CoordsHudCommand(
    private val playerDao: PlayerDao,
    private val textSerializer: ITextSerializer,
    private val config: CoordsHudConfig,
    private val state: HudState,
    commandManager: ICommandManager,
) {

    private fun toggleEnabled(player: ServerPlayerEntity) {
        val isEnabled = playerDao.getPlayer(player.uuid)
            .let { !it.isEnabled(config) }

        playerDao.setEnabled(player.uuid, isEnabled)

        if (!isEnabled) {
            // when coords hud is turned off, immediately clear the title
            player.sendMessage(Text.empty(), true)
        } else {
            // remove any delay when enabling the HUD
            state.titleTracker.remove(player)
        }
    }

    private fun setPlayerFormat(player: ServerPlayerEntity, text: Text?) {
        val json = text?.let { textSerializer.toJson(it) }
        playerDao.setFormat(player.uuid, json)
    }

    init {
        commandManager.register("coordshud") {
            executes {
                toggleEnabled(player ?: throw IllegalArgumentException())
            }

            literal("format") {
                text("text") { textArg ->
                    executes {
                        val player = player ?: throw IllegalArgumentException()
                        val text = getArgument(textArg)
                        setPlayerFormat(player, text)
                    }
                }

                literal("default") {
                    executes {
                        val player = player ?: throw IllegalArgumentException()
                        setPlayerFormat(player, null)
                    }
                }
            }
        }
    }

}
