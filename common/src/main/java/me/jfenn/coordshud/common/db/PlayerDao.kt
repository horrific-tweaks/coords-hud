package me.jfenn.coordshud.common.db

import me.jfenn.coordshud.common.cache.PlayerCache
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.upsert
import java.util.*

class PlayerDao(
    private val db: Database,
    private val cache: PlayerCache,
) {

    fun getPlayer(uuid: UUID): PlayerRecord = cache.players.get(uuid) {
        transaction(db) {
            PlayerTable.select(PlayerTable.format, PlayerTable.isEnabled)
                .where(PlayerTable.uuid eq uuid)
                .singleOrNull()
                ?.let {
                    PlayerRecord(
                        uuid = uuid,
                        isEnabled = it[PlayerTable.isEnabled],
                        format = it[PlayerTable.format],
                    )
                }
                ?: PlayerRecord(
                    uuid = uuid,
                    isEnabled = PlayerTable.isEnabled.defaultValueFun!!(),
                    format = PlayerTable.format.defaultValueFun!!(),
                )
        }
    }

    fun setEnabled(uuid: UUID, isEnabled: Boolean) = transaction(db) {
        PlayerTable.upsert {
            it[PlayerTable.uuid] = uuid
            it[PlayerTable.isEnabled] = isEnabled
        }
    }.also {
        cache.remove(uuid)
    }

    fun setFormat(uuid: UUID, format: String?) = transaction(db) {
        PlayerTable.upsert {
            it[PlayerTable.uuid] = uuid
            it[PlayerTable.format] = format
        }
    }.also {
        cache.remove(uuid)
    }

}