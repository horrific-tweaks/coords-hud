package me.jfenn.coordshud.common.config

import me.jfenn.coordshud.common.MOD_ID
import net.fabricmc.loader.api.FabricLoader
import org.slf4j.Logger

class ConfigService(
    private val log: Logger,
) {

    private val configPath = FabricLoader.getInstance().configDir.resolve("$MOD_ID/config.json")

    fun readConfig() : CoordsHudConfig {
        val configFile = configPath.toFile()
        if (configFile.exists()) {
            try {
                val configStr = configPath.toFile().readText()
                return json.decodeFromString<CoordsHudConfig>(configStr)
            } catch (e: Exception) {
                log.error("Error reading $configPath:", e)
            }
        }

        return CoordsHudConfig()
            .also { writeConfig(it) }
    }

    private fun writeConfig(config: CoordsHudConfig) {
        val configJson = json.encodeToString(config)
        configPath.toFile().parentFile.mkdirs()
        configPath.toFile().writeText(configJson)
    }

}