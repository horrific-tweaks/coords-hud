package me.jfenn.coordshud.common.hud

import net.minecraft.server.network.ServerPlayerEntity
import java.util.*

class HudState {
    // Track sent HUD messages to avoid re-sending the same one on each tick
    val titleTracker = WeakHashMap<ServerPlayerEntity, Pair<Int, String>>()
}