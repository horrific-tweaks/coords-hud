package me.jfenn.coordshud.common.cache

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import me.jfenn.coordshud.common.db.PlayerRecord
import net.minecraft.text.Text
import java.util.*
import java.util.concurrent.TimeUnit

class PlayerCache {
    val players: Cache<UUID, PlayerRecord> = CacheBuilder.newBuilder()
        .maximumSize(1000)
        .expireAfterWrite(10, TimeUnit.MINUTES)
        .build()

    val playerFormats: Cache<UUID, Text> = CacheBuilder.newBuilder()
        .maximumSize(1000)
        .expireAfterWrite(10, TimeUnit.MINUTES)
        .build()

    fun remove(playerId: UUID) {
        players.invalidate(playerId)
        playerFormats.invalidate(playerId)
    }
}