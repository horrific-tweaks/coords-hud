package me.jfenn.coordshud.common.db

import me.jfenn.coordshud.api.ITextSerializer
import me.jfenn.coordshud.common.config.CoordsHudConfig
import net.minecraft.text.Text
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table
import org.slf4j.Logger
import java.util.*

object PlayerTable : Table() {
    val uuid: Column<UUID> = uuid("uuid")
    val isEnabled: Column<Boolean?> = bool("is_enabled").nullable().default(null)
    val format: Column<String?> = varchar("format", 512).nullable().default(null)

    override val primaryKey = PrimaryKey(uuid)
}

data class PlayerRecord(
    val uuid: UUID,
    val isEnabled: Boolean?,
    val format: String?,
) {

    private var formatText: Text? = null

    fun getFormatText(
        textSerializer: ITextSerializer,
        logger: Logger,
    ) : Text? {
        return formatText
            ?: try {
                this.format
                    ?.let { textSerializer.fromJson(it) }
                    ?.also { this.formatText = it }
            } catch (e: Throwable) {
                logger.error("Error parsing custom hud format:", e)
                null
            }
    }

    fun isEnabled(config: CoordsHudConfig): Boolean {
        return this.isEnabled ?: config.isEnabledByDefault
    }

}
