package me.jfenn.coordshud.api

import net.minecraft.text.Text

interface ITextSerializer {
    fun toJson(text: Text): String
    fun fromJson(json: String): Text
}