package me.jfenn.coordshud.api

import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text
import net.minecraft.util.Identifier

interface IPlaceholderApi {

    fun parseText(format: Text, player: ServerPlayerEntity): Text

    fun registerPlaceholder(id: Identifier, callback: (ServerPlayerEntity) -> Text)

}