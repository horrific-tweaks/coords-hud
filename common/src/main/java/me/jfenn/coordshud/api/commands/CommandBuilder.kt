package me.jfenn.coordshud.api.commands

import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text

class CommandBuilder(
    private val parent: CommandNode
) {

    fun literal(name: String, configure: CommandBuilder.() -> Unit) {
        val node = CommandNode.Literal(name)
        CommandBuilder(node).configure()
        parent.children += node
    }

    fun text(name: String, configure: CommandBuilder.(arg: CommandArgument<Text>) -> Unit) {
        val arg = CommandArgument.Text(name)
        val node = CommandNode.RequiredArgument(arg)
        CommandBuilder(node).configure(arg)
        parent.children += node
    }

    fun executes(callback: IExecutionContext.() -> Unit) {
        parent.callback = callback
    }
}

sealed class CommandNode {
    val children = mutableListOf<CommandNode>()
    var callback: (IExecutionContext.() -> Unit)? = null

    class Root : CommandNode()
    class Literal(val name: String) : CommandNode()
    class RequiredArgument<T>(val arg: CommandArgument<T>) : CommandNode()
}

sealed class CommandArgument<T> {
    abstract val name: String

    class Text(
        override val name: String,
    ) : CommandArgument<net.minecraft.text.Text>()
}

interface IExecutionContext {
    val player: ServerPlayerEntity?
    fun <T> getArgument(arg: CommandArgument<T>): T
}
