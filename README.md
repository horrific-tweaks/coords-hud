# Coords HUD

[![Requires Fabric API](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/requires/fabric-api_vector.svg)](https://modrinth.com/mod/fabric-api)
[![Available on GitLab](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/available/gitlab_vector.svg)](https://gitlab.com/horrificdev/coords-hud)
[![Available on Modrinth](https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/cozy/available/modrinth_vector.svg)](https://modrinth.com/mod/coords-hud)

This is a small server-side mod to provide fast and configurable HUD text to players. It's very similar to the "Coordinates HUD" datapack from [Vanilla Tweaks](https://vanillatweaks.net/picker/datapacks/)!

![Text above the player's hotbar, showing coordinates, a compass direction, and the current time.](https://gitlab.com/horrificdev/coords-hud/-/raw/main/docs/screenshot.png)

It aims to provide some small improvements over the datapack:

- Allows custom server or player-specific HUD messages, using the [Placeholder API](https://placeholders.pb4.eu)
- Stores data in a config/db file, rather than including it in the world save
- More efficient for large servers, avoiding expensive processing and reducing sent packets

This was primarily built so that [Minecraft BINGO](https://minecraft.horrific.dev/bingo/) players don't need to re-run the `/trigger ch_toggle` command every time the server restarts with a new world. The customization with placeholders is a nice advantage.

While the performance improvements are good for large servers, the difference should not be noticeable for most players. If you aren't using the extra functionality in this mod, I'd recommend sticking with the datapack for better compatibility.

## ⌨️ Commands

- `/coordshud` toggles whether the HUD is enabled
- `/coordshud format <text>` changes the text format used for the HUD

## 📖 Configuration

On the first startup, the mod will create its config files in `./config/coords-hud/...` No initial setup is needed to use the mod; it should run out of the box.

- `"isEnabledByDefault"` (default: true) - Whether the HUD is enabled by default for new players.
- `"formatDefault"` - The default HUD text format, in [Raw JSON text format](https://minecraft.wiki/w/Raw_JSON_text_format) syntax.
