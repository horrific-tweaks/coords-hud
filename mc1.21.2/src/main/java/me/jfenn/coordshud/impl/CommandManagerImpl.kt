package me.jfenn.coordshud.impl

import com.mojang.brigadier.builder.ArgumentBuilder
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import com.mojang.brigadier.builder.RequiredArgumentBuilder
import com.mojang.brigadier.context.CommandContext
import me.jfenn.coordshud.api.commands.*
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback
import net.minecraft.command.CommandRegistryAccess
import net.minecraft.command.argument.TextArgumentType
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.server.network.ServerPlayerEntity
import org.slf4j.Logger

class CommandManagerImpl(
    private val log: Logger,
) : ICommandManager {

    private val root = CommandNode.Root()

    override fun register(configure: CommandBuilder.() -> Unit) {
        CommandBuilder(root).configure()
    }

    private fun constructCommand(
        command: CommandNode,
        registryAccess: CommandRegistryAccess,
    ): ArgumentBuilder<ServerCommandSource, *> {
        var builder: ArgumentBuilder<ServerCommandSource, *> = when (command) {
            is CommandNode.Literal -> LiteralArgumentBuilder.literal(command.name)
            is CommandNode.RequiredArgument<*> -> {
                when (command.arg) {
                    is CommandArgument.Text ->  RequiredArgumentBuilder.argument(command.arg.name, TextArgumentType.text(registryAccess))
                }
            }
            is CommandNode.Root -> throw IllegalArgumentException("Root nodes must not be provided within the tree!")
        }

        for (child in command.children) {
            val childCommand: ArgumentBuilder<ServerCommandSource, *> = constructCommand(child, registryAccess)

            @Suppress("UNCHECKED_CAST")
            builder = builder.then(childCommand) as ArgumentBuilder<ServerCommandSource, *>
        }

        if (command.callback != null) {
            builder.executes { ctx ->
                try {
                    command.callback?.invoke(ExecutionContextImpl(ctx))
                    1
                } catch (e: Throwable) {
                    log.error("Error in command handler:", e)
                    0
                }
            }
        }

        return builder
    }

    init {
        CommandRegistrationCallback.EVENT.register { dispatcher, registryAccess, _ ->
            for (command in root.children) {
                val builder = constructCommand(command, registryAccess) as? LiteralArgumentBuilder<ServerCommandSource>
                    ?: throw IllegalArgumentException("Commands on the root node must only be literal()!")

                dispatcher.register(builder)
            }
        }
    }
}

class ExecutionContextImpl(
    private val ctx: CommandContext<ServerCommandSource>,
) : IExecutionContext {
    override val player: ServerPlayerEntity?
        get() = ctx.source.player

    override fun <T> getArgument(arg: CommandArgument<T>): T {
        @Suppress("UNCHECKED_CAST")
        return when (arg) {
            is CommandArgument.Text -> TextArgumentType.getTextArgument(ctx, arg.name) as T
        }
    }
}
