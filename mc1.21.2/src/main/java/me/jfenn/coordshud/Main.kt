package me.jfenn.coordshud

import me.jfenn.coordshud.common.MOD_ID
import me.jfenn.coordshud.common.commonModule
import me.jfenn.coordshud.impl.CommandManagerImpl
import me.jfenn.coordshud.impl.PlaceholderApi
import me.jfenn.coordshud.impl.TextSerializer
import net.fabricmc.api.ModInitializer
import org.slf4j.LoggerFactory

class Main : ModInitializer {

    private val log = LoggerFactory.getLogger(MOD_ID)

    override fun onInitialize() {
        log.info("Starting Coords HUD")

        commonModule(
            logger = log,
            placeholderApi = PlaceholderApi(log),
            textSerializer = TextSerializer(),
            commandManager = CommandManagerImpl(log),
        )
    }
}